// FastBiot_GPU3D_v1:  Wave propagation in anisotropic poroelastic media using graphical processing units (GPUs);
// Solving the elastodynamic Biot's equations in orthorhombic media
// This routine is written on CUDA C for a single GPU.
// 25 Jan 2021
// Biot, M. A. (1962). Mechanics of deformation and acoustic propagation in porous media. Journal of applied physics, 33(4), 1482-1498.
// Copyright (C) 2021  Yury Alkhimenkov, Ludovic Raess, Yury Podladchikov.

// Please cite us if you use our routine: 
// Alkhimenkov Y., Raess L., Khakimova L., Quintal B., Podladchikov Y.Y., 2021. 
// Resolving wave propagation in anisotropic poroelastic media using graphical processing units (GPUs)

// Output: This code calculates Vx, Vy, Vz wave fields and saves them. 
// Input:  Parameters are generated in the Matlab script "vizme_FastBiot_GPU3D"; 3 files are needed --- pa1, pa2, pa2
// To use: You need a GPU with 2.6-3.0 GB of GPU DRAM; Better to use Cuda/9.0 or above
// To run and visualize: Use the Matlab script "vizme_FastBiot_GPU3D".
// Or, insted of using "vizme_FastBiot_GPU3D", to run manually in the terminal: 
// to compile:  nvcc -arch=sm_52 -O3 -DNBX=4 -DNBY=4 -DNBZ=4 -DOVERX=0 -DOVERY=0 -DOVERZ=0 -Dnt=300 -DNPARS1=10 -DNPARS2=13 -DNPARS3=15  FastBiot_GPU3D_v1.cu
// to run:      ./a.out
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define GPU_ID 0
// #define USE_SINGLE_PRECISION      /* Comment this line using "!" if you want to use double precision.  */
#ifdef USE_SINGLE_PRECISION
#define DAT     float
#define MPI_DAT MPI_REAL
#define PRECIS  4
#else
#define DAT     double
#define MPI_DAT MPI_DOUBLE_PRECISION
#define PRECIS  8
#endif
////////// ========== Simulation Initialisation ========== //////////
#define BLOCKS_X    32  
#define BLOCKS_Y    2  
#define BLOCKS_Z    8  
#define GRID_X      (NBX*2) 
#define GRID_Y      (NBY*32) 
#define GRID_Z      (NBZ*8) 

// maximum overlap in x, y, z direction. x : Vx is nx+1, so it is 1; y: Vy is ny+1, so it is 1; z: Vz is nz+1, so it is 1.
#define MAX_OVERLENGTH_X OVERX //3
#define MAX_OVERLENGTH_Y OVERY //3
#define MAX_OVERLENGTH_Z OVERZ //3

// Numerics
const int nx     = GRID_X*BLOCKS_X - MAX_OVERLENGTH_X;        // we want to have some threads available for all cells of any array, also the ones that are bigger than nx.
const int ny     = GRID_Y*BLOCKS_Y - MAX_OVERLENGTH_Y;        // we want to have some threads available for all cells of any array, also the ones that are bigger than ny.
const int nz     = GRID_Z*BLOCKS_Z - MAX_OVERLENGTH_Z;        // we want to have some threads available for all cells of any array, also the ones that are bigger than nz.
//const int nt     =  10;
const int nout   =  1; 
const int niter  =  10;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Definition of basic macros
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define NB_THREADS             (BLOCKS_X*BLOCKS_Y*BLOCKS_Z)
#define NB_BLOCKS              (GRID_X*GRID_Y*GRID_Z)
#define def_sizes(A,nx,ny,nz)  const int sizes_##A[] = {nx,ny,nz};                            
#define size(A,dim)            (sizes_##A[dim-1])
#define numel(A)               (size(A,1)*size(A,2)*size(A,3))
#define end(A,dim)             (size(A,dim)-1)
#define zeros_h(A,nx,ny,nz)    def_sizes(A,nx,ny,nz);                                  \
                               DAT *A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                               for(i=0; i < (nx)*(ny)*(nz); i++){ A##_h[i]=(DAT)0.0; }
#define zeros(A,nx,ny,nz)      def_sizes(A,nx,ny,nz);                                         \
                               DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                               for(i=0; i < (nx)*(ny)*(nz); i++){ A##_h[i]=(DAT)0.0; }        \
                               cudaMalloc(&A##_d      ,numel(A)*sizeof(DAT));                 \
                               cudaMemcpy( A##_d,A##_h,numel(A)*sizeof(DAT),cudaMemcpyHostToDevice);
#define ones(A,nx,ny,nz)       def_sizes(A,nx,ny,nz);                                         \
                               DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(numel(A)*sizeof(DAT)); \
                               for(i=0; i < (nx)*(ny)*(nz); i++){ A##_h[i]=(DAT)1.0; }        \
                               cudaMalloc(&A##_d      ,numel(A)*sizeof(DAT));                 \
                               cudaMemcpy( A##_d,A##_h,numel(A)*sizeof(DAT),cudaMemcpyHostToDevice);
#define gather(A)              cudaMemcpy( A##_h,A##_d,numel(A)*sizeof(DAT),cudaMemcpyDeviceToHost);
#define free_all(A)            free(A##_h);cudaFree(A##_d);
#define swap(A,B,tmp)          DAT *tmp; tmp = A##_d; A##_d = B##_d; B##_d = tmp;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Variables for cuda
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int nprocs=1, me=0;
dim3 grid, block;
int gpu_id=-1;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions (host code)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void set_up_gpu(){
    block.x = BLOCKS_X; block.y = BLOCKS_Y; block.z = BLOCKS_Z;
    grid.x  = GRID_X;   grid.y  = GRID_Y;   grid.z  = GRID_Z;
    gpu_id  = GPU_ID;
    cudaSetDevice(gpu_id); cudaGetDevice(&gpu_id);
    cudaDeviceReset();                                // Reset the device to avoid problems caused by a crash in a previous run (does still not assure proper working in any case after a crash!).
    cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);  // set L1 to prefered
}

void clean_cuda(){ 
    cudaError_t ce = cudaGetLastError();
    if(ce != cudaSuccess){ printf("ERROR launching GPU C-CUDA program: %s\n", cudaGetErrorString(ce)); cudaDeviceReset();}
}
// Timer 
#include "sys/time.h"
double timer_start = 0;
double cpu_sec(){ struct timeval tp; gettimeofday(&tp,NULL); return tp.tv_sec+1e-6*tp.tv_usec; }
void   tic(){ timer_start = cpu_sec(); }
double toc(){ return cpu_sec()-timer_start; }
void   tim(const char *what, double n){ double s=toc();if(me==0){ printf("%s: %8.3f seconds",what,s);if(n>0)printf(", %8.3f GB/s", n/s); printf("\n"); } }
////////// ========== Save & Read Data functions ========== //////////
/// Params to be saved for evol plot ///
#define NB_PARAMS  1
void save_info(){
    FILE* fid;
    if (me==0){ fid=fopen("0_infos.inf", "w"); fprintf(fid,"%d %d %d %d %d %d",PRECIS,nx,ny,nz,NB_PARAMS,(int)ceil((DAT)niter/(DAT)nout));  fclose(fid);}
}

void save_array(DAT* A, size_t nb_elems, const char A_name[], int isave){
    char* fname; FILE* fid;
    asprintf(&fname, "%d_%d_%s.res" ,isave, me, A_name); 
    fid=fopen(fname, "wb"); fwrite(A, PRECIS, nb_elems, fid); fclose(fid); free(fname);
}
#define SaveArray(A,A_name)  gather(A); save_array(A##_h, numel(A), A_name, isave);

void read_data(DAT* A_h, DAT* A_d, int nx,int ny,int nz, const char A_name[], const char B_name[],int isave){
    char* bname; size_t nb_elems = nx*ny*nz; FILE* fid;
    asprintf(&bname, "%d_%d_%s.%s", isave, me, A_name, B_name);
    fid=fopen(bname, "rb"); // Open file
    if (!fid){ fprintf(stderr, "\nUnable to open file %s \n", bname); return; }
    fread(A_h, PRECIS, nb_elems, fid); fclose(fid);
    cudaMemcpy(A_d, A_h, nb_elems*sizeof(DAT), cudaMemcpyHostToDevice);
    if (me==0) printf("Read data: %d files %s.%s loaded (size = %dx%dx%d) \n", nprocs,A_name,B_name,nx,ny,nz); free(bname);
}

void read_data_h(DAT* A_h, int nx, int ny,int nz, const char A_name[], const char B_name[],int isave){
    char* bname; size_t nb_elems = nx*ny*nz; FILE* fid;
    asprintf(&bname, "%d_%d_%s.%s", isave, me, A_name, B_name);
    fid=fopen(bname, "rb"); // Open file
    if (!fid){ fprintf(stderr, "\nUnable to open file %s \n", bname); return; }
    fread(A_h, PRECIS, nb_elems, fid); fclose(fid);
    if (me==0) printf("Read data: %d files %s.%s loaded (size = %dx%dx%d) \n", nprocs,A_name,B_name,nx,ny,nz); free(bname);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define Dx  ( (DAT)1.0/dx )
#define Dy  ( (DAT)1.0/dy )
#define Dz  ( (DAT)1.0/dz )

#define load(A,nx,ny,Aname) double *A##_d,*A##_h; A##_h = (double*)malloc((nx)*(ny)*sizeof(double));  \
                            FILE* A##fid=fopen(Aname, "rb"); fread(A##_h, sizeof(double), (nx)*(ny), A##fid); fclose(A##fid); \
                            cudaMalloc(&A##_d,((nx)*(ny))*sizeof(double)); \
                            cudaMemcpy(A##_d,A##_h,((nx)*(ny))*sizeof(double),cudaMemcpyHostToDevice);  
#define  swap(A,B,tmp)      DAT *tmp; tmp = A##_d; A##_d = B##_d; B##_d = tmp;

// Computing physics kernels /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void init(DAT* x, DAT* y,DAT* z, DAT* Prf, DAT* sigma_xx, DAT* sigma_yy, DAT* sigma_zz, DAT* Vx, DAT* Vy, DAT* Qxft, DAT* Qyft, const DAT dx, const DAT dy,const DAT dz, const DAT Lx, const DAT Ly,const DAT Lz, const DAT lamx, const DAT lamy,const DAT lamz, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    if (iz<nz && iy<ny && ix<nx){ x[ix + iy*nx + iz*nx*ny] = (DAT)ix*dx - (DAT)0.5*Lx; }
    if (iz<nz && iy<ny && ix<nx){ y[ix + iy*nx + iz*nx*ny] = (DAT)iy*dy - (DAT)0.5*Ly; }
    if (iz<nz && iy<ny && ix<nx){ z[ix + iy*nx + iz*nx*ny] = (DAT)iz*dz - (DAT)0.5*Lz; }
    if (iz<nz && iy<ny && ix<nx){ Prf[ix + iy*nx + iz*nx*ny] = -(DAT)10000000000.0*exp(  -(x[ix + iy*nx + iz*nx*ny]*x[ix + iy*nx + iz*nx*ny]/lamx/lamx) -(y[ix + iy*nx + iz*nx*ny]*y[ix + iy*nx + iz*nx*ny]/lamy/lamy) -(z[ix + iy*nx + iz*nx*ny]*z[ix + iy*nx + iz*nx*ny]/lamz/lamz)  ); }
}

__global__ void compute_StressPrf(DAT* Prf, DAT* sigma_xx, DAT* sigma_yy,DAT* sigma_zz, DAT* sigma_xy, DAT* sigma_xz, DAT* sigma_yz, DAT* Vx, DAT* Vy,DAT* Vz, DAT* Qxft, DAT* Qyft,DAT* Qzft, const DAT dx, const DAT dy,const DAT dz,const DAT dt,const DAT c11u,const DAT c22u,const DAT c33u,const DAT c12u,const DAT c13u,const DAT c23u,const DAT c44u,const DAT c55u,const DAT c66u,const DAT alpha1,const DAT alpha2,const DAT alpha3,const DAT M1, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    #define diffVx  (   (Vx[(ix+1) + (iy  )*(nx+1) + (iz  )*(nx+1)*(ny  )]   -  Vx[ix + iy*(nx+1) + iz*(nx+1)*(ny  )]   )*((DAT)1.0/dx)   )
    #define diffVy  (   (Vy[(ix  ) + (iy+1)*(nx  ) + (iz  )*(nx  )*(ny+1)]   -  Vy[ix + iy*(nx  ) + iz*(nx  )*(ny+1)]   )*((DAT)1.0/dy)   )
    #define diffVz  (   (Vz[(ix  ) + (iy  )*(nx  ) + (iz+1)*(nx  )*(ny  )]   -  Vz[ix + iy*(nx  ) + iz*(nx  )*(ny  )]   )*((DAT)1.0/dz)   )
    #define div_Qf  ( (((DAT)1.0/dx)*(Qxft[(ix+1) + (iy  )*(nx+1) + (iz  )*(nx+1)*(ny  )]-Qxft[ix + iy*(nx+1) + iz*(nx+1)*(ny  )])  + ((DAT)1.0/dy)*(Qyft[(ix  ) + (iy+1)*(nx  ) + (iz  )*(nx  )*(ny+1)]-Qyft[ix + iy*(nx  ) + iz*(nx  )*(ny+1)]) + ((DAT)1.0/dz)*(Qzft[(ix  ) + (iy  )*(nx  ) + (iz+1)*(nx  )*(ny  )]-Qzft[ix + iy*(nx  ) + iz*(nx  )*(ny  )]) )    )

    if (iz<nz && iy<ny && ix<nx){
        DAT Vxx = diffVx;
        DAT Vyy = diffVy;
        DAT Vzz = diffVz;
        DAT Qff = div_Qf;
        sigma_xx[ix + iy*nx + iz*nx*ny]     = sigma_xx[ix + iy*nx + iz*nx*ny] + dt*(c11u* Vxx + c12u* Vyy + c13u* Vzz + alpha1*M1*Qff); 
        sigma_yy[ix + iy*nx + iz*nx*ny]     = sigma_yy[ix + iy*nx + iz*nx*ny] + dt*(c12u* Vxx + c22u* Vyy + c23u* Vzz + alpha2*M1*Qff); 
        sigma_zz[ix + iy*nx + iz*nx*ny]     = sigma_zz[ix + iy*nx + iz*nx*ny] + dt*(c13u* Vxx + c23u* Vyy + c33u* Vzz + alpha3*M1*Qff); 
        Prf[ix + iy*nx + iz*nx*ny] = Prf[ix + iy*nx + iz*nx*ny] + dt*(  -alpha1* M1* Vxx  - alpha2* M1* Vyy - alpha3* M1* Vzz - M1* Qff );
    }
    if (iz<nz && iy>0 && iy<ny && ix>0 && ix<nx){ sigma_xy[ix + iy*(nx+1) + iz*(nx+1)*(ny+1)] = sigma_xy[ix + iy*(nx+1) + iz*(nx+1)*(ny+1)] + c66u*dt*( (Vy[ix + iy* nx + iz*nx  *(ny+1)] - Vy[ix-1 + iy* nx + iz*nx*(ny+1)  ])*((DAT)1.0/dx) + (Vx[ix + iy* (nx+1)+ iz*(nx+1)*(ny-0)] - Vx[ix  +(iy-1)*(nx+1) +  iz   *(nx+1)*ny])*((DAT)1.0/dy) ); }
    if (iz>0 && iz<nz && iy<ny && ix>0 && ix<nx){ sigma_xz[ix + iy*(nx+1) + iz*(nx+1)* ny   ] = sigma_xz[ix + iy*(nx+1) + iz*(nx+1)* ny   ] + c55u*dt*( (Vz[ix + iy* nx + iz*nx  * ny   ] - Vz[ix-1 + iy* nx + iz*nx* ny     ])*((DAT)1.0/dx) + (Vx[ix + iy* (nx+1)+ iz*(nx+1)* ny   ] - Vx[ix + iy* (nx+1)    + (iz-1)*(nx+1)*ny])*((DAT)1.0/dz) ); }
    if (iz>0 && iz<nz && iy>0 && iy<ny && ix<nx){ sigma_yz[ix + iy* nx    + iz* nx   *(ny+1)] = sigma_yz[ix + iy* nx    + iz*nx*(ny+1)    ] + c44u*dt*( (Vy[ix + iy* nx + iz*nx*  (ny+1)] - Vy[ix + iy* nx + (iz-1)*nx*(ny+1)])*((DAT)1.0/dz) + (Vz[ix + iy* nx    + iz*nx*ny        ] - Vz[ix + (iy-1)* nx    +  iz   * nx   *ny])*((DAT)1.0/dy) ); }

    #undef diffVx
    #undef diffVy
    #undef diffVz
    #undef div_Qf
}

__global__ void update_Qxft(DAT* Prf, DAT* sigma_xx, DAT* sigma_yy,DAT* sigma_zz, DAT* sigma_xy, DAT* sigma_xz, DAT* sigma_yz, DAT* Vx, DAT* Vy,DAT* Vz, DAT* Qxft, DAT* Qyft,DAT* Qzft,DAT* Qxold,DAT* Qyold,DAT* Qzold, const DAT dx, const DAT dy,const DAT dz,const DAT dt,const DAT eta_k1,const DAT eta_k2,const DAT eta_k3,const DAT mm1,const DAT mm2,const DAT mm3,const DAT delta1,const DAT delta2,const DAT delta3,const DAT rho_fluid1,const DAT rho_fluid2,const DAT rho_fluid3,const DAT rho1,const DAT rho2,const DAT rho3, const int nx, const int ny, const int nz, const DAT chi){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    #define div_Sigmax  ( ( sigma_xx[ix + iy*nx + iz*nx*ny]-sigma_xx[(ix-1) + (iy  )*nx + (iz  )*nx*ny] )*((DAT)1.0/dx) + ( sigma_xy[ix   + (iy+1)*(nx+1) + iz*(nx+1)*(ny+1)] - sigma_xy[ix + iy*(nx+1) + iz*(nx+1)*(ny+1)])*((DAT)1.0/dy) + (sigma_xz[ix + (iy+0)*(nx+1) + (iz+1)* (nx+1)*(ny+0)] - sigma_xz[ix + iy*(nx+1) + iz*(nx+1)*(ny+0)])*((DAT)1.0/dz) )
    #define div_Sigmay  ( ( sigma_yy[ix + iy*nx + iz*nx*ny]-sigma_yy[(ix  ) + (iy-1)*nx + (iz  )*nx*ny] )*((DAT)1.0/dy) + ( sigma_xy[ix+1 + (iy  )*(nx+1) + iz*(nx+1)*(ny+1)] - sigma_xy[ix + iy*(nx+1) + iz*(nx+1)*(ny+1)])*((DAT)1.0/dx) + (sigma_yz[ix + (iy  )*(nx+0) + (iz+1)* (nx+0)*(ny+1)] - sigma_yz[ix + iy*(nx+0) + iz*(nx+0)*(ny+1)])*((DAT)1.0/dz) )
    #define div_Sigmaz  ( ( sigma_zz[ix + iy*nx + iz*nx*ny]-sigma_zz[(ix  ) + (iy  )*nx + (iz-1)*nx*ny] )*((DAT)1.0/dz) + ( sigma_xz[ix+1 + (iy  )*(nx+1) + iz*(nx+1)*(ny+0)] - sigma_xz[ix + iy*(nx+1) + iz*(nx+1)*(ny+0)])*((DAT)1.0/dx) + (sigma_yz[ix + (iy+1)*(nx+0) +  iz   * (nx+0)*(ny+1)] - sigma_yz[ix + iy*(nx+0) + iz*(nx+0)*(ny+1)])*((DAT)1.0/dy) )
    #define Q_gradPrfx  ( ( Prf[ix + iy*nx + iz*nx*ny]  -Prf[(ix-1) + (iy  )*nx + (iz  )*nx*ny] )*((DAT)1.0/dx) + ((DAT)1.0-chi )*( Qxold[ix + iy*(nx+1) +  iz*  (nx+1)*ny   ] )*eta_k1)
    #define Q_gradPrfy  ( ( Prf[ix + iy*nx + iz*nx*ny]  -Prf[(ix  ) + (iy-1)*nx + (iz  )*nx*ny] )*((DAT)1.0/dy) + ((DAT)1.0-chi )*( Qyold[ix + iy*(nx  ) + (iz  )*nx*  (ny+1)] )*eta_k2)
    #define Q_gradPrfz  ( ( Prf[ix + iy*nx + iz*nx*ny]  -Prf[(ix  ) + (iy  )*nx + (iz-1)*nx*ny] )*((DAT)1.0/dz) + ((DAT)1.0-chi )*( Qzold[ix + iy*(nx  ) +  iz*   nx*   ny   ] )*eta_k3)
    
    if (iz<nz && iy<ny && ix>0 && ix<nx){
        DAT QPx = Q_gradPrfx;
        DAT dSx = div_Sigmax;
        Qxft[ix + iy*(nx+1) + iz*(nx+1)*(ny  )] = ( Qxold[ix + iy*(nx+1) + iz*(nx+1)*(ny  )]*((DAT)1.0/dt) - rho1* delta1* QPx - rho_fluid1* delta1* dSx )* (  (DAT)1.0/( ((DAT)1.0/dt) + chi*rho1* delta1*eta_k1 ));
        Vx[ix + iy*(nx+1) + iz*(nx+1)*(ny  )]   = ( Vx[ix + iy*(nx+1) + iz*(nx+1)*(ny  )]*((DAT)1.0/dt) + mm1* delta1* dSx + rho_fluid1* delta1* ( QPx + chi*eta_k1*Qxft[ix + iy*(nx+1) + iz*(nx+1)*(ny  )] )  )*dt;
    }   
    if (iz<nz && iy>0 && iy<ny && ix<nx){
        DAT QPy = Q_gradPrfy;
        DAT dSy = div_Sigmay;
        Qyft[ix + iy*(nx  ) + iz*(nx  )*(ny+1)] = ( Qyold[ix + iy*(nx  ) + iz*(nx  )*(ny+1)]*((DAT)1.0/dt) - rho2* delta2* QPy - rho_fluid2* delta2* dSy )* (  (DAT)1.0/( ((DAT)1.0/dt) + chi*rho2* delta2*eta_k2 )  );
        Vy[ix + iy*(nx  ) + iz*(nx  )*(ny+1)]   = ( Vy[ix + iy*(nx  ) + iz*(nx  )*(ny+1)]*((DAT)1.0/dt) + mm2* delta2* dSy + rho_fluid2* delta2* ( QPy + chi*eta_k2*Qyft[ix + iy*(nx  )   + iz*(nx  )*(ny+1)] )  )*dt;
    }
    if (iz>0 && iz<nz && iy<ny && ix<nx){
        DAT QPz = Q_gradPrfz;
        DAT dSz = div_Sigmaz;
        Qzft[ix + iy*(nx  ) + iz*(nx  )*(ny  )] = ( Qzold[ix + iy*(nx  ) + iz*(nx  )*(ny  )]*((DAT)1.0/dt) - rho3* delta3* QPz - rho_fluid3* delta3* dSz )* (  (DAT)1.0/( ((DAT)1.0/dt) + chi*rho3* delta3*eta_k3 )  );
        Vz[ix + iy*(nx  ) + iz*(nx  )*(ny  )]   = ( Vz[ix + iy*(nx  ) + iz*(nx  )*(ny  )]*((DAT)1.0/dt) + mm3* delta3* dSz + rho_fluid3* delta3* ( QPz + chi*eta_k3*Qzft[ix + iy*(nx  )   + iz*(nx  )*(ny  )] )  )*dt;
    }

    #undef div_Sigmax
    #undef div_Sigmay
    #undef div_Sigmaz
    #undef Q_gradPrfx
    #undef Q_gradPrfy
    #undef Q_gradPrfz
    #undef Q_gradPrfx1
    #undef Q_gradPrfy1
    #undef Q_gradPrfz1
}
////////// ========================================  MAIN  ======================================== //////////
int main(){
    size_t i, N;
    int it;
    N = nx*ny*nz; double mem = (double)1e-9*(double)N*sizeof(DAT);
    set_up_gpu();
    printf("\n  -------------------------------------------------------------------------- ");
    printf("\n  | FastBiot_GPU3D_v1:  Wave propagation in anisotropic poroelastic media  | ");
    printf("\n  --------------------------------------------------------------------------  \n\n");
    printf("Local size: %dx%dx%d (%1.4f GB) %d iterations ...\n", nx, ny, nz, mem*19.0, nt);
    printf("Launching (%dx%dx%d) grid of (%dx%dx%d) blocks.\n\n", grid.x, grid.y, grid.z, block.x, block.y, block.z);
    // Load input parameters
    load(pa1,NPARS1 , 1,"pa1.dat")
    load(pa2,NPARS2 , 1,"pa2.dat")
    load(pa3,NPARS3 , 1,"pa3.dat") //dt   =pa1_h[3]
    double dx  =pa1_h[0],dy  =pa1_h[1],dz  =pa1_h[2],dt  =pa1_h[3],Lx  =pa1_h[4],Ly  =pa1_h[5],Lz  =pa1_h[6],lamx=pa1_h[7],lamy  =pa1_h[8],lamz   =pa1_h[9]; 
    double c11u=pa2_h[0],c33u=pa2_h[1],c13u=pa2_h[2],c12u=pa2_h[3],c23u=pa2_h[4],c44u=pa2_h[5],c55u=pa2_h[6],c66u=pa2_h[7],alpha1=pa2_h[8],alpha2 =pa2_h[9],alpha3=pa2_h[10],M1=pa2_h[11],c22u=pa2_h[12]; 
    double eta_k1=pa3_h[0],eta_k2=pa3_h[1],eta_k3=pa3_h[2],mm1=pa3_h[3],mm2=pa3_h[4],mm3=pa3_h[5],delta1=pa3_h[6],delta2 =pa3_h[7],delta3=pa3_h[8],rho_fluid1=pa3_h[9],rho_fluid2=pa3_h[10],rho_fluid3 =pa3_h[11],rho1=pa3_h[12],rho2=pa3_h[13],rho3=pa3_h[14];
    const DAT chi    = (DAT)0.5;
    // Initial arrays
    zeros(x        ,nx  ,ny  ,nz  );
    zeros(y        ,nx  ,ny  ,nz  );
    zeros(z        ,nx  ,ny  ,nz  );
    zeros(Vx       ,nx+1,ny  ,nz  );
    zeros(Vy       ,nx  ,ny+1,nz  );
    zeros(Vz       ,nx  ,ny  ,nz+1);
    zeros(sigma_xx ,nx  ,ny  ,nz  );
    zeros(sigma_yy ,nx  ,ny  ,nz  );
    zeros(sigma_zz ,nx  ,ny  ,nz  );
    zeros(sigma_xy ,nx+1,ny+1,nz  );
    zeros(sigma_xz ,nx+1,ny  ,nz+1);
    zeros(sigma_yz ,nx  ,ny+1,nz+1);
    zeros(Prf      ,nx  ,ny  ,nz  );
    zeros(Qxft     ,nx+1,ny  ,nz  );
    zeros(Qyft     ,nx  ,ny+1,nz  );
    zeros(Qzft     ,nx  ,ny,nz+1  );
    zeros(Qxold    ,nx+1,ny  ,nz  );
    zeros(Qyold    ,nx  ,ny+1,nz  );
    zeros(Qzold    ,nx  ,ny,nz+1  );
    // Initial condition
    int isave=0; //  ON
    // Initial conditions
    init<<<grid,block>>>(x_d, y_d, z_d, Prf_d, sigma_xx_d, sigma_yy_d, sigma_zz_d, Vx_d, Vy_d, Qxft_d, Qyft_d, dx, dy,dz, Lx, Ly,Lz,lamx, lamy,lamz, nx, ny, nz); cudaDeviceSynchronize();
    // Action
    for (it=0;it<nt;it++){
        if (it==51){ tic(); }
        compute_StressPrf<<<grid,block>>>(Prf_d, sigma_xx_d, sigma_yy_d,sigma_zz_d, sigma_xy_d, sigma_xz_d, sigma_yz_d, Vx_d, Vy_d,Vz_d, Qxft_d, Qyft_d,Qzft_d,dx, dy, dz, dt, c11u,c22u, c33u, c12u,c13u,c23u,         c44u,c55u,c66u, alpha1,alpha2, alpha3, M1, nx, ny, nz);  
        cudaDeviceSynchronize();

        swap(Qxold, Qxft, tmp11); swap(Qyold, Qyft, tmp22); swap(Qzold, Qzft, tmp33); 
        cudaDeviceSynchronize();

        update_Qxft<<<grid,block>>>(Prf_d, sigma_xx_d, sigma_yy_d,sigma_zz_d, sigma_xy_d, sigma_xz_d, sigma_yz_d, Vx_d, Vy_d,Vz_d, Qxft_d, Qyft_d,Qzft_d,Qxold_d,Qyold_d,Qzold_d, dx, dy,dz, dt, eta_k1,            eta_k2,eta_k3, mm1,mm2, mm3, delta1,delta2, delta3, rho_fluid1,rho_fluid2,rho_fluid3, rho1,rho2,rho3, nx, ny, nz, chi);
        cudaDeviceSynchronize();
    }
    tim("Performance", mem*(nt-50)*42); // timer
    printf("Process %d used GPU with id %d.\n", me, gpu_id);
    ///////////================================================================================ POSTPROCESS ====////
    save_info();  // Save simulation infos and coords (.inf files)
    SaveArray(Vx   , "Vx"  )
    SaveArray(Vy   , "Vy"  )
    SaveArray(Vz   , "Vz"  )
    // clear host memory & clear device memory
    free_all(x);
    free_all(y);
    free_all(z);
    free_all(Vx);
    free_all(Vy);
    free_all(Vz);
    free_all(sigma_xx);
    free_all(sigma_yy);
    free_all(sigma_zz);
    free_all(sigma_xy);
    free_all(sigma_xz);
    free_all(sigma_yz);
    free_all(Prf);
    free_all(Qxft);
    free_all(Qyft);
    free_all(Qzft);
    free_all(Qxold);
    free_all(Qyold);
    free_all(Qzold);
    clean_cuda();
    return 0;
}
