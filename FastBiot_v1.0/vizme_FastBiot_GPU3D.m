% FastBiot:  wave propagation in anisotropic poroelastic media using graphical processing units (GPUs);
% Solving the elastodynamic Biot's equations in orthorhombic media
% 07 Feb 2021

% This script is developed to run the CUDA C routine "FastBiot_GPU3D_v1.cu" and visualize the results

% Biot, M. A. (1962). Mechanics of deformation and acoustic propagation in porous media. Journal of applied physics, 33(4), 1482-1498.
% Copyright (C) 2021 - Yury Alkhimenkov, Ludovic Raess, Yury Podladchikov.

% Please cite us if you use our routine:
% Alkhimenkov Y., Raess L., Khakimova L., Quintal B., Podladchikov Y.Y., 2021.
% Resolving wave propagation in anisotropic poroelastic media using graphical processing units (GPUs)

% This script:
% 1) creates parameter files (pa1, pa2, pa3);
% 2) compiles and runs the code on a GPU 'FastBiot_GPU3D_v1'
% 3) visualize and save the result: 2D plot of Vx and 3D plot of V_total

%% Sample output
% vizme_FastBiot_GPU3D
% 
%   -------------------------------------------------------------------------- 
%   | FastBiot_GPU3D_v1:  Wave propagation in anisotropic poroelastic media  | 
%   --------------------------------------------------------------------------  
% 
% Local size: 256x256x256 (2.5501 GB) 300 iterations ...
% Launching (8x128x32) grid of (32x2x8) blocks.
% 
% Performance:    2.219 seconds,  634.968 GB/s
% Process 0 used GPU with id 0.
% GPU_time =
%     7.7303
% Vx(150,150,150) = 0.299101135419527597303357424607384018599987030029
% Vy(150,150,150) = 0.300455337084786555656990003626560792326927185059
% Vz(150,150,150) = -0.546700282247006619229523494141176342964172363281
%%

clear
format compact
% NBX is the input parameter to GPU
NBX = 4; 
NBY = 4; 
NBZ = 4;  
nt  = 300; % number of iterations

BLOCK_X  = 32; % BLOCK_X*BLOCK_Y<=1024
BLOCK_Y  = 2;
BLOCK_Z  = 8;
GRID_X   = NBX*2;
GRID_Y   = NBY*32;
GRID_Z   = NBZ*8;

OVERLENGTH_X = 0;
OVERLENGTH_Y = OVERLENGTH_X;
OVERLENGTH_Z = OVERLENGTH_X;

nx = BLOCK_X*GRID_X  - OVERLENGTH_X; % size of the model in x
ny = BLOCK_Y*GRID_Y  - OVERLENGTH_Y; % size of the model in y
nz = BLOCK_Z*GRID_Z  - OVERLENGTH_Z; % size of the model in z

OVERX = OVERLENGTH_X;
OVERY = OVERLENGTH_Y;
OVERZ = OVERLENGTH_Z;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Physics
% parameters with independent units
spatial_scale = 1;
Lx     = 9.35/spatial_scale; % m
Ly     = 9.35/spatial_scale; % m
Lz     = 9.35/spatial_scale; % m
K_dry  = 0.2 + 2/3*0.4;      % Bulk m. Pa
G0     = 0.4;                % shear m.
c      = zeros(6,6);
%Set the medium
% 1 --- Isotropic medium
% 2 --- Orthorhombic medium (Glass/Epoxy)
Medium_type = 2;
%% Isotropic rock
if Medium_type == 1
    % elastic constants:
    c(1,1)    = 0.2 + 2*0.4;
    c(2,2)    = 0.2 + 2*0.4;
    c(1,3)    = 0.2;
    c(1,2)    = 0.2;
    c(2,3)    = 0.2;
    c(3,3)    = 0.2 + 2*0.4;
    c(4,4)    = G0;
    c(5,5)    = G0;
    c(6,6)    = G0;
    
    Tor1      = 2.0; % tortuosity
    Tor2      = 2.0; % tortuosity
    Tor3      = 2.0; % tortuosity
    
    rho_solid = 1815;  % solid density
    fi        = 0.2;   % porosity
    rho_fluid = 1040 ; % fluid density
    K_g       = 40e9;  % solid bulk m.
    K_fl      = 2.5e9; % fluid bulk m.
    k_etaf1   = 600*10^(-15) /(1e-3); % permeability/viscosity
    k_etaf2   = 600*10^(-15) /(1e-3); % permeability/viscosity
    k_etaf3   = 600*10^(-15) /(1e-3); % permeability/viscosity
    invisc    = 1; % medium is not inviscid
end
%% Glass/Epoxy
if Medium_type == 2
    % elastic constants:
    c(1,1)    = 39.4e9;
    c(1,2)    = 1.0e9;
    c(1,3)    = 5.8e9;
    c(2,2)    = 39.4e9;
    c(2,3)    = 5.8e9;
    c(3,3)    = 13.1e9;
    c(4,4)    = 3.0e9;
    c(5,5)    = 3.0e9;
    c(6,6)    = 2.0e9;
    
    Tor1      = 2.0; % tortuosity
    Tor2      = 2.0; % tortuosity
    Tor3      = 3.6; % tortuosity
    
    rho_solid = 1815;  % solid density
    fi        = 0.2;   % porosity
    rho_fluid = 1040;  % fluid density
    K_g       = 40e9;  % solid bulk m.
    K_fl      = 2.5e9; % fluid bulk m.
    k_etaf1   = 600*10^(-15) /(1e-3); % permeability/viscosity
    k_etaf2   = 600*10^(-15) /(1e-3); % permeability/viscosity
    k_etaf3   = 100*10^(-15) /(1e-3); % permeability/viscosity
    invisc    = 1; % medium is not inviscid
end
%% Spatial size of the initial condition
lamx    = Lx/spatial_scale/50;
lamy    = Ly/spatial_scale/50;
lamz    = Lz/spatial_scale/50;
%% Calculation of more complex parameters
beta_g  = 1/K_g;
beta_f  = 1/K_fl;
alpha1  = 1 - (c(1,1) + c(1,2) + c(1,3)).*beta_g/3; % Biot coef
alpha2  = 1 - (c(1,3) + c(2,2) + c(2,3)).*beta_g/3; % Biot coef
alpha3  = 1 - (c(1,3) + c(1,3) + c(3,3)).*beta_g/3; % Biot coef
rho     = (1-fi)*rho_solid + fi*rho_fluid;
M1      = (K_g.^2) ./ (    K_g.*(1 + fi.*(K_g/K_fl - 1) ) - ( 2.*c(1,1) + c(3,3) + 2*c(1,2) + 4*c(1,3) )/9    );
%%
beta_d  = 1/K_dry;
alphaIS = 1 - beta_g./beta_d;
GPU_x   = (beta_d - beta_g) / (beta_d - beta_g + fi*(beta_f - beta_g));
K_u     = K_dry/(1 - GPU_x*alphaIS );
M_is    = GPU_x*K_u/alphaIS; %M1     = M_is; to double-check
%%
M11     = (K_g.^2) ./ (    K_g.*(1 + fi.*(K_g./K_fl - 1) ) - ( c(1,1)+c(2,2) + c(3,3)+ 2.*(c(1,2) + c(1,3)  +c(2,3)) )./9    );
%%
c11u    = c(1,1) + alpha1.^2*M1;
c22u    = c(2,2) + alpha2.^2*M1;
c33u    = c(3,3) + alpha3.^2*M1;
c13u    = c(1,3) + alpha1*alpha3*M1;
c12u    = c(1,2) + alpha1*alpha2*M1;
c23u    = c(2,3) + alpha2*alpha3*M1;
c44u    = c(4,4);
c55u    = c(5,5);
c66u    = c(6,6);

mm1     = rho_fluid*Tor1/fi;
mm2     = rho_fluid*Tor2/fi;
mm3     = rho_fluid*Tor3/fi;
delta1  = rho.*mm1 - rho_fluid.^2;
delta2  = rho.*mm2 - rho_fluid.^2;
delta3  = rho.*mm3 - rho_fluid.^2;

eta_k1  = 1./k_etaf1;
eta_k2  = 1./k_etaf2;
eta_k3  = 1./k_etaf3;

dx      = Lx/(nx-1);
dy      = Ly/(ny-1);
dz      = Lz/(nz-1);
%%
iM_ELan = [c11u alpha1*M1;alpha1*M1 M1];
iMdvp   = [mm1 rho_fluid; rho_fluid rho]./delta1;
A11     = iM_ELan(1,1);    A12 = iM_ELan(1,2);   A22 = iM_ELan(2,2); % elast
R11     = iMdvp(1,1); R12 = iMdvp(1,2);R22 = iMdvp(2,2); %densit
A3_m    = -(R11 * R22 - R12 ^ 2) * (A11 * A22 - A12 ^ 2);
A2_m    =  (A11 * R11 - 2 * A12 * R12 + A22 * R22) ;
Solution1inf  = 1./ (( -A2_m + (A2_m.*A2_m + 4.*A3_m)^0.5 )./2./A3_m ).^0.5;
Vp_HF   = Solution1inf;
%%
Vp      = sqrt(c11u/rho);
dt_sound = 1/sqrt(1./dx.^2 + 1./dy.^2 + 1./dz.^2) * 1/Vp_HF ;
dt      = dt_sound*0.6;
%% Coolecting input parameters for C-Cuda
delta1_av     = 1./delta1;
delta2_av     = 1./delta2;
delta3_av     = 1./delta3;
eta_k1_av     = eta_k1;
eta_k2_av     = eta_k2;
eta_k3_av     = eta_k3;
rho_fluid1_av = rho_fluid;
rho_fluid2_av = rho_fluid;
rho_fluid3_av = rho_fluid;
rho1_av       = rho;
rho2_av       = rho;
rho3_av       = rho;
pa1           = [dx dy dz dt Lx Ly Lz lamx lamy lamz];
pa2           = [c11u c33u c13u c12u c23u c44u c55u c66u alpha1 alpha2 alpha3 M1 c22u];
pa3           = [eta_k1_av eta_k2_av eta_k3_av mm1 mm2 mm3 delta1_av delta2_av delta3_av...
    rho_fluid1_av rho_fluid2_av rho_fluid3_av rho1_av rho2_av rho3_av];
fid           = fopen('pa1.dat','wb'); fwrite(fid,pa1(:),'double'); fclose(fid);
fid           = fopen('pa2.dat','wb'); fwrite(fid,pa2(:),'double'); fclose(fid);
fid           = fopen('pa3.dat','wb'); fwrite(fid,pa3(:),'double'); fclose(fid);
%% Running on GPU
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
code_name    = 'FastBiot_GPU3D_v1';
run_cmd      =['nvcc -arch=sm_70 -O3' ...
    ' -DNBX='    int2str(NBX)         ...
    ' -DNBY='    int2str(NBY)         ...
    ' -DNBZ='    int2str(NBZ)         ...
    ' -DOVERX='  int2str(OVERX)       ...
    ' -DOVERY='  int2str(OVERY)       ...
    ' -DOVERZ='  int2str(OVERZ)       ...
    ' -Dnt='     int2str(nt)          ...
    ' -DNPARS1=' int2str(length(pa1)) ...
    ' -DNPARS2=' int2str(length(pa2)) ...
    ' -DNPARS3=' int2str(length(pa3)) ' ',code_name,'.cu'];

system(run_cmd);
tic;
! ./a.out
GPU_time = toc

%% to run from the terminal
% nvcc -arch=sm_52 -O3 -DNBX=4 -DNBY=4 -DNBZ=4 -DOVERX=0 -DOVERY=0 -DOVERZ=0 -Dnt=300 -DNPARS1=10 -DNPARS2=13 -DNPARS3=15  GPU3D_Biot_v2.cu
%% Reading data
% Load the DATA and infos
isave = 0;
infos = load('0_infos.inf');  PRECIS=infos(1); nx=infos(2); ny=infos(3); nz=infos(4); NB_PARAMS=infos(5); NB_EVOL=infos(6);
if (PRECIS==8), DAT = 'double';  elseif (PRECIS==4), DAT = 'single';  end
name=[num2str(isave) '_0_Vx.res']; id = fopen(name); Vx  = fread(id,DAT); fclose(id); Vx  = reshape(Vx  ,nx+1,ny  ,nz  );
name=[num2str(isave) '_0_Vy.res']; id = fopen(name); Vy  = fread(id,DAT); fclose(id); Vy  = reshape(Vy  ,nx  ,ny+1,nz  );
name=[num2str(isave) '_0_Vz.res']; id = fopen(name); Vz  = fread(id,DAT); fclose(id); Vz  = reshape(Vz  ,nx  ,ny  ,nz+1);

%% 2D plot, Vx, Vy, Vz
figure(1),clf,
S1 = subplot(1,1,1);surf(Vx(:,:,fix(nx/2)));
%colormap(S1,gray(1000)), axis square tight
colormap(S1,Red_blue_colormap), axis square tight
view(0,90);
colorbar
shading interp;title('Vx');caxis([-0.6; 0.6]);

disp(['Vx(150,150,150) = ' , num2str(Vx(150,150,150),'%50.48f') ]);
disp(['Vy(150,150,150) = ' , num2str(Vy(150,150,150),'%50.48f') ]);
disp(['Vz(150,150,150) = ' , num2str(Vz(150,150,150),'%50.48f') ]);

figname = 'Vx';
fig = gcf;    fig.PaperPositionMode = 'auto';
print([figname '_' int2str(0)],'-dpng','-r600')
%% 3D plot, V_total
figure(2), clf
Vx_plot  = (Vx(2:end,:,:) + Vx(1:end-1,:,:))/2;
Vy_plot  = (Vy(:,2:end,:) + Vy(:,1:end-1,:))/2;
Vz_plot  = (Vz(:,:,2:end) + Vz(:,:,1:end-1))/2;
Vx_norm  = Vx_plot + Vy_plot + Vz_plot;
clf,set(gcf,'color','white','pos',[700 200 1000 800]);
S1 = figure(2);clf; hold on;
s1 = fix( nx/2  );
s2 = fix( ny/2  );
s3 = fix( nz/10    );
s4 = fix( nz/2  );
s5 = fix( nz      );
slice(Vx_norm( 1:s2  , 1:s2  ,1:s5),[],s2,[]),shading flat
slice(Vx_norm(1:s2, :  ,1:s5),s2,[],[]),shading flat
slice(Vx_norm( :  ,1:s1,1:s4),[],nx,[]),shading flat
slice(Vx_norm(1:s2, 1:s2  , :  ),[],[],s5),shading flat
slice(Vx_norm( :  ,1:s1, :  ),[],[],s4),shading flat
slice(Vx_norm( :  , :  ,1:s4),s1,[],[]),shading flat

s7=fix( nx/10  );s8 = fix( nz/1.3  );
slice(Vx_norm( :  ,1:s7,1:s8),[],nx,[]),shading flat
slice(Vx_norm( :  ,1:s7, :  ),[],[],s8),shading flat
slice(Vx_norm( :  , :  ,1:s8),s7,[],[]),shading flat

slice(Vx_norm( :  , :  ,1:s3),ny,[],[]),shading flat
slice(Vx_norm( :  , :  ,1:s3),[],nx,s3),shading flat

s7=fix( nx/10  );s8 = fix( nz  );
slice(Vx_norm( :  ,1:s7,1:s8),[],nx,[]),shading flat
slice(Vx_norm( :  ,1:s7, :  ),[],[],s8),shading flat
slice(Vx_norm( :  , :  ,1:s8),s7,[],[]),shading flat

isosurf = 0.4;
is1  = isosurface(Vx_plot, isosurf);
is2  = isosurface(Vx_plot, -isosurf);
his1 = patch(is1); set(his1,'CData',+isosurf,'Facecolor','Flat','Edgecolor','none')
his2 = patch(is2); set(his2,'CData',-isosurf,'Facecolor','Flat','Edgecolor','none')

is1y  = isosurface(Vy_plot, isosurf);
is2y  = isosurface(Vy_plot, -isosurf);
his1y = patch(is1y); set(his1y,'CData',+isosurf,'Facecolor','Flat','Edgecolor','none')
his2y = patch(is2y); set(his2y,'CData',-isosurf,'Facecolor','Flat','Edgecolor','none')

isosurfz = 0.4;
is1z  = isosurface(Vz_plot, isosurfz);
is2z  = isosurface(Vz_plot, -isosurfz);
his1z = patch(is1z); set(his1z,'CData',+isosurf,'Facecolor','Flat','Edgecolor','none')
his2z = patch(is2z); set(his2z,'CData',-isosurf,'Facecolor','Flat','Edgecolor','none')

hold off; box on;xlabel('x (m)'); ylabel('y (m)');  zlabel('z (m)');
axis image; view(138,27)

camlight; camproj perspective
light('position',[0.6 -1 1]);     light('position',[0.8 -1.0 1.0]);
pos = get(gca,'position'); set(gca,'position',[0.01 pos(2), pos(3), pos(4)])
h = colorbar; caxis([-0.9; 0.9]);
daspect([1,1,1]);  axis square; grid on;
cb = colorbar; set(cb,'position',[0.78, 0.16, 0.04, 0.165])
title(cb,'V_{total} (m/s)');title('V_{total} (m/s)')

set(cb,'YTick',-0.8:0.8:0.8);sc_ph = Lx/(nx -1);
centX = ( fix(nx/Lx/1*8.675) - fix(nx/Lx/1*0.675))/4 ;

set(gca, 'XTick', (  fix(nx/Lx/1*0.675)  :  centX  :  fix(1022/Lx/1*8.675)) ,'fontsize',16 );
xticklabels({'4','2','0','-2','-4'})
set(gca, 'YTick',(  fix(nx/Lx/1*0.675)  :  centX  :  fix(1022/Lx/1*8.675)) ,'fontsize',16 );
yticklabels({'4','2','0','-2','-4'})
set(gca, 'ZTick', (  fix(nx/Lx/1*0.675)  :  centX  :  fix(1022/Lx/1*8.675)) ,'fontsize',16 );
zticklabels({'-4','-2','0','2','4'})
grid on;colormap(S1,Red_blue_colormap);
ax = gca;ax.BoxStyle = 'full';
box on;ax.LineWidth = 2;

figname = 'Vt_total';
fig = gcf;    fig.PaperPositionMode = 'auto';
print([figname '_' int2str(0)],'-dpng','-r600')
%% delete the data
End_of_the_code = 1;
delete *.res *.inf *.dat a.out
