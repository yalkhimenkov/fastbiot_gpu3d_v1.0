%%
% Biot_dispersion non-dimensional: this script calculates the dispersion
% relations of the non-dimensional Biot's equations
% Copyright (C) 2021  Yury Alkhimenkov, Ludovic Raess, Lyudmila Khakimova, Beatriz Quintal, Yury Podladchikov.

% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version. 

% Please cite us if you use our routine: 
% Alkhimenkov Y., Raess L., Khakimova L., Quintal B., Podladchikov Y.Y., 2021. 
% Resolving wave propagation in anisotropic poroelastic media using graphical processing units (GPUs)
%%
tic;opengl software
figure(2);clear, clc;clf; 
format long; format compact; 
%%
%%
%%
%% NON-DIMENSIONAL dispersion relations
%%
%%
%%
%%
%% Physics 
% parameters with independent units
visc        = 1e-3;        % viscosity
perm        = 1e-12 ;      % permeability
etaf_k      = visc/perm ;  % viscosity / permeability => [Pa*s/m^2]
rho_s       = 2700;        % solid density  [kg/m^3]
K_dry       = 26e9;        % Bulk modulus of the frame [Pa]
% nondimentional parameters
fi          = 0.30;           % porosity [-]
rho_fluid_rho_solid = 0.4;    % ratio
K_g__K_dry  = 1.42;           % ratio
Mu_g__Mu_dry= 1.42;           % ratio
Kf_K_dry    = 0.0865;         % ratio
Tor         = 1.9;            % tortuosity [-]
G0_K_dry    = 15/13;
% dimentionally dependent parameters
G0          = G0_K_dry*K_dry;      % shear modulus of the frame [Pa]
c11         = K_dry+ 4/3*G0;       % c11 dry
K_g         = K_g__K_dry*K_dry;    % solid grain material [Pa]
Mu_g        = Mu_g__Mu_dry*G0;     % solid shear grain material [Pa]
K_fl        = Kf_K_dry*K_dry;      % fluid bulk modulus   [Pa]
Tor_fi      = Tor/fi;              % shortcut
beta_d      = 1./K_dry;            % compliance
beta_g      = 1./K_g;              % compliance
beta_f      = 1./K_fl;             % compliance
alpha       = 1 - beta_g./beta_d;  % Biot alpha 
B           = (beta_d - beta_g) ./ (beta_d - beta_g + fi.*(beta_f - beta_g)); % Biot B
rho_f       = rho_fluid_rho_solid.*rho_s; % fluid density [kg/m^3]
rho_t       = (1-fi).*rho_s + fi.*rho_f;  % total density 
K_u         = K_dry./(1 - B*alpha );      % K_u undrained
MM          = B.*K_u./alpha;              % M
%% dimensional parameters. Constructing matrices
M_EL       = [ 1 , -alpha; -alpha, (alpha/B + 4/3*G0/MM) ] ./ c11;
%iM_ELmum   = inv(M_EL); % for validation
iM_ELan    = [  (alpha/B + 4/3*G0/MM), alpha; alpha,  1]./ (  alpha/B + 4/3*G0/MM  -alpha^2) .* c11;
rho_ft  = rho_f/rho_t; rho_at = rho_f*Tor_fi/rho_t; 
Mdvp       = rho_t.*[1, -rho_ft; -rho_ft, +rho_at];
iMdvp      = inv(Mdvp);
rho_a      = rho_f*Tor_fi;
Pe         = 1/etaf_k;
c11        = K_dry+ 4/3*G0; c11_save = K_dry +4/3*G0; rho_t_save = rho_t; V_d = sqrt((K_dry +4/3*G0)/rho_t);
%% some useful values
V_dry1     = ((K_dry+ 4/3*G0) / ((1-fi).*rho_s + fi.*rho_f) )^0.5;
omegaT     = etaf_k*fi/rho_f/Tor;
%% making matrices non-dimensional
c11        = 1; rho_t = 1; 
etaf_k_save= etaf_k;
etaf_k     = 1;
M_EL       = [ 1 , -alpha; -alpha, (alpha/B + 4/3*G0/MM) ] ./ c11;
iM_ELmum   = inv(M_EL);
iM_ELan    = [  (alpha/B + 4/3*G0/MM), alpha; alpha,  1]./ (  alpha/B + 4/3*G0/MM  -alpha^2) .* c11;
Mdvp       = rho_t.*[1, -rho_ft; -rho_ft, +rho_at];
iMdvp      = inv(Mdvp);
%% Dispersion relations non-dimensional
figure(2); clf;
timerCoun = 0;
omega1    = logspace(2.0,10.0,1000)/1000000; 
for freq1 = 1:1e6/1000:1e6
    timerCoun = timerCoun +1;
    omega     =omega1(timerCoun);  
%% The same as in the paper JGR:SE
I1 = 1; I2 = etaf_k;  S = 1/c11; a = alpha; aA = alpha/B*(1 + 4/3*G0/K_u);
R12_20 = rho_ft; R22_20 = rho_at;

A0_m = S ^ 2 * I1 ^ 2 * (R12_20 ^ 2 - R22_20) * (a ^ 2 - aA) * omega ^ 4 - 1i * S ^ 2 * I2 * I1 * (a ^ 2 - aA) * omega ^ 3;
A2_m = ( (-2*a * R12_20 + aA * R22_20  + 1) * S * I1 * omega ^ 2) + 1i * I2 * S * aA * omega;
A3_m = 1;

s_squared            =  ( A2_m - (A2_m.*A2_m - 4.*A3_m.*A0_m)^0.5 )./2./A3_m;
QQ1(timerCoun)       = imag(s_squared)./real(s_squared);

s_squared2           =  (( A2_m + (A2_m.*A2_m - 4.*A3_m.*A0_m)^0.5 )./2./A3_m );
QQ2(timerCoun)       = imag(s_squared2)./real(s_squared2);%.^2
Solution1(timerCoun) = omega*1./real( (( A2_m - (A2_m.*A2_m - 4.*A3_m.*A0_m)^0.5 )./2./A3_m ).^0.5);
Solution2(timerCoun) = omega*1./real( (( A2_m + (A2_m.*A2_m - 4.*A3_m.*A0_m)^0.5 )./2./A3_m ).^0.5);
end

subplot(2,1,1);  semilogx(omega1,real(Solution1) ,'linewidth',3); hold on; %.*V_d
semilogx(omega1,real(Solution2) ,'linewidth',3);%.*V_d
legend({'\it{V_1}','\it{V_2}'},...
    'FontSize', 20);%ylim([0 1.1]);
grid on;ylabel('Non-dimensional velocity (-)', 'FontSize', 12);xlabel('\omega (-)', 'FontSize', 12);xlim([10^-3.5 10^3.5]);ylim([0 1.2]);%xlim([1e-3 1e4]);
subplot(2,1,2);  semilogx( omega1 ,QQ1,'linewidth',3); hold on; %loglog
subplot(2,1,2);  semilogx( omega1 ,QQ2,'linewidth',3);
grid on;
ylabel('1/Q (-)', 'FontSize', 12)%, 'interpreter', 'latex'
xlabel('\omega (-)', 'FontSize', 12)%, 'interpreter', 'latex' (Hz)
ylim([0 0.05]);
xlim([10^-3.5 10^3.5]);

%% printing some useful values (non-dimensional)
format long g
omegaT = etaf_k*fi/rho_f/Tor
V_HF = real(Solution1(end))  % velocity undrained high freq
V_LF = real(Solution1(1))    % velocity undrained low freq
Vdu  = (iM_ELan(1,1)/rho_t).^0.5 % velocity undrained low freq (validation)
Vd   = (c11/rho_t).^0.5      % velocity drained low freq